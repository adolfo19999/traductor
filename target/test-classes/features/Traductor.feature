Feature: Traducir un texto con google

  Scenario: Yo como usuario necesito ingresar al traductor de google y traducir un texto pequeño
    Given Yo abro el navegador y busco google
    And luego escribo traductor y presiono enter
    When encuentra el traductor
    And hago clic en el traductor
    And ingreso el texto a traducir
    Then Entrega la traduccion del texto