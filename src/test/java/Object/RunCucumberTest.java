package Object;



import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"Object"},
        features = "src/test/resources/features")

public class RunCucumberTest {
}
