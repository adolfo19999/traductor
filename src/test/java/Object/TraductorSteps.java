package Object;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TraductorSteps {
    public static void main (String [] args) {}
        WebDriver driver;
        @Given("Yo abro el navegador y busco google")
        public void yoAbroElNavegadorYBuscoGoogle () {
            driver = new FirefoxDriver();
            driver.get("https://google.cl");
        }


        @And("luego escribo traductor y presiono enter")
        public void luegoEscriboTraductorYPresionoEnter () throws InterruptedException {

            WebElement escribir = driver.findElement(By.xpath("//input[@title='Buscar']"));
            escribir.sendKeys("traductor");
            escribir.sendKeys(Keys.ENTER);
            Thread.sleep(2000);
        }

        @When("encuentra el traductor")
        public void encuentraElTraductor () {
        }

        @And("hago clic en el traductor")
        public void hagoClicEnElTraductor () {
            driver.findElement(By.xpath("//*[@id=\"tw-source-text-ta\"]")).click();
        }

        @And("ingreso el texto a traducir")
        public void ingresoElTextoATraducir () throws InterruptedException {
            WebElement eso = driver.findElement(By.xpath("//textarea[contains(@placeholder,'Ingresar texto')]"));
            eso.sendKeys("Hi, I'm Adolfo and I'm automating what is required.");
            Thread.sleep(10000);

        }

        @Then("Entrega la traduccion del texto")
        public void entregaLaTraduccionDelTexto () {
            WebElement element = driver.findElement(By.xpath("//div[@class='g9WsWb'][contains(.,'Hola, soy Adolfo y estoy automatizando lo que se requiere.       Verificado       Los colaboradores de Google Traductor verificaron esta traducción    Leer más         Cómo se pronuncia    Traducción copiada')]"));
            String resultado = "Hola, soy Adolfo y estoy automatizando lo que se requiere.";
            Assert.assertEquals(resultado, element.getText());
            driver.quit();
        }

}
